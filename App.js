import React, { Component } from 'react'
import Index from './src/index'
import * as firebase from 'firebase'

firebase.initializeApp({
    apiKey: "AIzaSyAhmRKG-4fmNzLho-eD4JTVn2S8BdptcZE",
    authDomain: "movie-catalog-firebase.firebaseapp.com",
    databaseURL: "https://movie-catalog-firebase.firebaseio.com",
    projectId: "movie-catalog-firebase",
    storageBucket: "movie-catalog-firebase.appspot.com",
    messagingSenderId: "899149944216",
    appId: "1:899149944216:web:4169f88c68c685847387ed",
    measurementId: "G-9FT4FMKPMH"
 });

export default () => <Index />