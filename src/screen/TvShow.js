import React, { Component } from 'react'
import { 
    StyleSheet, 
    Text, 
    View,
    Image,
    TouchableOpacity,
    TextInput,
    FlatList,
} from 'react-native'
import axios from 'axios'
import TvShows from './component/TvItem'
import { connect } from 'react-redux'
import { ADD_MOVIE } from '../constants/actionTypes'

class TvShow extends Component {
    constructor(props){
        super(props)
        this.state = {
            data: [],
            baseUrl: 'https://api.themoviedb.org/3',
            apiKey: 'e852848561694f1300e3eac1deb19c49'
        }
    }

    componentDidMount = () => {
        axios.get(`${this.state.baseUrl}/discover/tv?api_key=${this.state.apiKey}`)
            .then(res => {
            const data = res.data.results;
            this.setState({ data });
            })
    }

    detailTv = (data) => {
        this.props.dispatch({
            type: ADD_MOVIE,
            payload: [data.name, data.first_air_date, data.popularity, data.vote_average, data.overview, data.poster_path]
        });
        this.props.navigation.navigate('Detail')
    }

    render = () => {
        return (
            <View style={styles.container}>
                <FlatList
                    data={this.state.data}
                    keyExtractor={(data) => data.id.toString()}
                    renderItem={(item) =>
                    <TvShows 
                        tv={item.item} 
                        onPress={() => this.detailTv(this.state.detail = item.item)}/>}
                    ItemSeparatorComponent={() => <View style={{height:0.5,backgroundColor:'#E5E5E5'}}/>}
                />
            </View>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        justifyContent: 'center',
    },
})

const mapStateToProps = (state) => ({
    todo: state.todo
})

export default connect(mapStateToProps)(TvShow)