import { StatusBar } from 'expo-status-bar'
import React, { Component } from 'react'
import { 
    StyleSheet, 
    Text, 
    View,
    Image,
    TouchableOpacity,
    TextInput,
} from 'react-native'
import Constants from 'expo-constants'
import Moment from 'moment'
import { connect } from 'react-redux'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'

class Detail extends Component {
    constructor(props){
        super(props)
    }

    render = () => {
        Moment.locale('en');
        const imageUrl = 'https://image.tmdb.org/t/p/original'
        return (
            <View style={styles.container}>
                <Image source={{ uri: `${imageUrl}${this.props.todo.poster}`}} style={styles.poster}/>
                <View style={styles.content}>
                    <Text style={styles.title} numberOfLines={4}>{this.props.todo.title}</Text>
                    <View style={styles.date}>
                        <MaterialCommunityIcons name='calendar' color='black' size={20}/>
                        <Text style={styles.textStyle}>{`Release Date : ${Moment(this.props.todo.release_date).format('dd, D MMM yyyy')}`}</Text>
                    </View>
                    <View style={styles.date}>
                        <FontAwesome5 name='users' color='black' size={20}/>                        
                        <Text style={styles.textStyle}>{`Popularity : ${this.props.todo.popularity}`}</Text>
                    </View>
                    <View style={styles.rating}>
                        <MaterialCommunityIcons name='star' color='black' size={20}/>
                        <Text style={styles.textStyle}>{`Rating : ${this.props.todo.rating}`}</Text>
                    </View>
                    <View style={styles.detail}>
                        <Text style={styles.textTitleContent}>{`Overview`}</Text>
                        <Text style={styles.textStyle}>{this.props.todo.overview}</Text>
                    </View>
                </View>
            </View>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#fff',
        borderRadius: 5,
        margin: 5
    },
    poster: {
        height: 300,
        width: 200,
        borderRadius: 10,
        marginHorizontal: 5,
        marginVertical: 5,
        alignSelf: 'center'
    },
    content: {
        flex: 1,
        flexDirection: 'column',
        marginHorizontal: 5
    },
    title: {
        fontWeight: 'bold',
        fontSize: 20,
        marginBottom: 5,
    },
    date: {
        flexDirection: 'row',
        marginBottom: 5,
    },
    cost: {
        flexDirection: 'row',
        marginBottom: 5,
    },
    rating: {
        flexDirection: 'row',
        marginBottom: 5,
    },
    detail: {
        flexDirection: 'column',
        marginBottom: 5,
        marginTop: 10
    },
    textStyle: {
        color: '#000000',
        marginHorizontal: 5,
        fontSize: 15
    },
    textTitleContent: {
        color: '#000000',
        marginHorizontal: 5,
        fontSize: 18,
        fontWeight: 'bold'
    },
})

const mapStateToProps = (state) => ({
    todo: state.todo
})

export default connect(mapStateToProps)(Detail)