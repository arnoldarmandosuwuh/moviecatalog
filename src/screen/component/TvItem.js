import React, { Component } from 'react'
import {
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity
} from 'react-native'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Moment from 'moment'

export default class TvItem extends Component {
    render = () => {
        Moment.locale('en');
        const imageUrl = 'https://image.tmdb.org/t/p/original'
        let tv = this.props.tv
        return(
            <View style={styles.container}>
                <Image source={{ uri: `${imageUrl}${tv.poster_path}`}} style={styles.poster}/>
                <View style={styles.content}>
                    <Text style={styles.title} numberOfLines={4}>{tv.name}</Text>
                    <View style={styles.date}>
                        <MaterialCommunityIcons name='calendar' color='black' size={20}/>
                        <Text style={styles.textStyle}>{Moment(tv.first_air_date).format('D MMM yyyy')}</Text>
                    </View>
                    <View style={styles.date}>
                        <FontAwesome5 name='users' color='black' size={20}/>                        
                        <Text style={styles.textStyle}>{tv.popularity}</Text>
                    </View>
                    <View style={styles.rating}>
                        <MaterialCommunityIcons name='star' color='black' size={20}/>
                        <Text style={styles.textStyle}>{tv.vote_average}</Text>
                    </View>
                    <View style={styles.detail}>
                        <TouchableOpacity style={styles.buttonStyle} onPress={this.props.onPress}>
                            <MaterialCommunityIcons name='eye' color='white' size={20}/>
                            <Text style={styles.buttonTextStyle}>Detail</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        backgroundColor: '#fff',
        borderRadius: 5,
        margin: 5
    },
    poster: {
        height: 300,
        width: 200,
        borderRadius: 10,
        marginHorizontal: 5
    },
    content: {
        flex: 1,
        flexDirection: 'column',
        marginHorizontal: 5
    },
    title: {
        fontWeight: 'bold',
        fontSize: 20,
        marginBottom: 5,
    },
    date: {
        flexDirection: 'row',
        marginBottom: 5,
    },
    cost: {
        flexDirection: 'row',
        marginBottom: 5,
    },
    rating: {
        flexDirection: 'row',
        marginBottom: 5,
    },
    detail: {
        flexDirection: 'row',
        marginBottom: 5,
        marginTop: 10
    },
    textStyle: {
        color: '#000000',
        marginHorizontal: 5
    },
    buttonTextStyle: {
        color: '#fff',
        fontSize: 15,
        marginHorizontal: 5
    },
    buttonStyle: {
        backgroundColor: '#4251f5',
        width: '70%',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        height: 40,
        borderRadius: 5,
        marginTop: 10,
        marginBottom: 10
    },
})