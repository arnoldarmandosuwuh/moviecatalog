import { StatusBar } from 'expo-status-bar'
import React, { Component } from 'react'
import { 
    StyleSheet, 
    Text, 
    View,
    Image,
    TouchableOpacity,
    TextInput,
    AsyncStorage,
} from 'react-native'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import * as firebase from 'firebase'
import { connect } from 'react-redux'
import ProgressBar from 'react-native-progress/Bar'
import { CREATE_SESSION } from '../constants/actionTypes'
import Constants from 'expo-constants'

class Login extends Component {
    constructor(props) {
        super(props)
        this.state = {
            email: '',
            password: '',
            isError: false,
            isLoading: false,
        }
    }

    loginHandler = () => {
        this.setState({isError:'', isLoading:true});

        const{email, password} = this.state;
        firebase.auth().signInWithEmailAndPassword(email, password)
        .then( async () => {
            this.setState({isError:false, isLoading:false});
            this.props.dispatch({
                type: CREATE_SESSION,
                payload: email
            })
            await AsyncStorage.setItem('email', email)
            this.props.navigation.navigate('Movie');
        })
        .catch(() => {
            this.setState({isError: true, isLoading:false});
        })
    }

    loading = () => {
        if (this.state.isLoading) {
            return <ProgressBar progress={0.3} width={200}/>
        }
    }

    render = () => {
        return (
            <View style={styles.container}>
                <Image source={require('../../assets/login.png')} style={styles.imageBackground}/>
                <View style={styles.title}>
                    <Text style={styles.titleText}>LOGIN</Text>
                </View>
                <View style={styles.formContainer}>
                    <View style={styles.inputContainer}>
                        <MaterialCommunityIcons name='account-circle' color='#FF0000' size={40} />
                        <View>
                            <TextInput 
                                style={styles.inputStyle} 
                                placeholder="Email" 
                                placeholderTextColor="#000"
                                onChangeText={email => this.setState({ email })}/>
                        </View>
                    </View>
                    <View style={styles.inputContainer}>
                        <MaterialCommunityIcons name='lock' color='#FF0000' size={40} />
                        <View>
                            <TextInput 
                                secureTextEntry={true}
                                style={styles.inputStyle} 
                                placeholder="Password" 
                                placeholderTextColor="#000"
                                onChangeText={password => this.setState({ password })}/>
                        </View>
                    </View>
                </View>
                <Text style={this.state.isError ? styles.errorText : styles.hiddenErrorText}>Email / Password Salah</Text>
                <TouchableOpacity 
                    style={styles.buttonStyle} 
                    onPress={() => this.loginHandler()}>
                    <Text style={styles.textStyle}>LOGIN</Text>
                </TouchableOpacity>
                {this.loading()}
                <StatusBar style="auto" />
            </View>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 2,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: Constants.statusBarHeight
    },
    imageBackground: {
        flex: 1,
        resizeMode: 'cover',
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        width: '100%',
		height: '100%'
    },
    title: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 50,
        marginBottom: 50,
    },
    titleText:{
        fontWeight: 'bold',
        fontSize: 40,
        color: '#fff',
        alignSelf: 'center',
        justifyContent: 'center'
    },
    formContainer: {
        justifyContent: "center",
    },
    inputContainer: {
        marginBottom: 16,
        flexDirection: 'row',
        alignItems: 'center',
        alignSelf: 'center',
        justifyContent: 'center'
    },
    inputStyle: {
        color: '#000000',
        backgroundColor: '#ffffff70',
        width: 250,
        borderRadius: 5,
        padding: 10,
        justifyContent: 'center'
    },
    labelText: {
        fontWeight: 'bold',
        color: '#FF5B00',
        alignSelf: 'center',
        fontSize: 24
    },
    buttonStyle: {
        backgroundColor: '#4CFCF1',
        width: '60%',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 5,
        height: 50,
        marginTop: 10,
        marginBottom: 10
    },
    textStyle: {
        fontWeight: 'bold',
        color: '#000000',
        fontSize: 14,
    },
    errorText: {
        color: 'red',
        textAlign: 'center',
        marginBottom: 16,
        fontWeight: 'bold'
    },
    hiddenErrorText: {
        color: 'transparent',
        textAlign: 'center',
        marginBottom: 16,
    }
})

const mapStateToProps = (state) => ({
    todo: state.todo
})

export default connect(mapStateToProps)(Login)