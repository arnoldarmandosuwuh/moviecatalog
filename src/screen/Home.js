import { StatusBar } from 'expo-status-bar'
import React, { Component } from 'react'
import Constants from 'expo-constants'
import { 
    StyleSheet, 
    Text, 
    View,
    Image,
    TouchableOpacity,
    TextInput,
    ImageBackground,
    Button,
    AsyncStorage
} from 'react-native'
import { connect } from 'react-redux'
import { CREATE_SESSION } from '../constants/actionTypes'

class Home extends Component {
    constructor(props) {
        super(props)
        this.state = {
            userName: '',
            password: '',
            isLogin: false,
        }
    }

    componentDidMount= () => {
       this.loginCheckAsync()
    }

    loginCheckAsync = async () => {
        const email = await AsyncStorage.getItem('email')

        if (email !== null){
            this.props.dispatch({
                type: CREATE_SESSION,
                payload: email
            })
            this.props.navigation.navigate('Movie')
        }
    }
    

    render = () => {
        return (
            <View style={styles.container}>
                <Image source={require('../../assets/background.png')} style={styles.imageBackground}/>
                <TouchableOpacity style={styles.buttonStyle} onPress={() => this.props.navigation.navigate('Login')}>
                    <Text style={styles.textStyle}>LOGIN</Text>
                </TouchableOpacity>
                <Text style={styles.textStyle}>OR</Text>
                <TouchableOpacity style={styles.buttonStyle} onPress={() => this.props.navigation.navigate('Register')}>
                    <Text style={styles.textStyle}>REGISTER</Text>
                </TouchableOpacity>
                <StatusBar style="auto" />
            </View>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 2,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: Constants.statusBarHeight
    },
    imageBackground: {
        flex: 1,
        resizeMode: 'cover',
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        width: '100%',
		height: '100%'
    },
    buttonStyle: {
        backgroundColor: '#24FF00',
        width: '60%',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 5,
        height: 50,
        marginTop: 10,
    },
    textStyle: {
        fontWeight: 'bold',
        color: '#fff',
        fontSize: 14,
    }
})

const mapStateToProps = (state) => ({
    todo: state.todo
})

export default connect(mapStateToProps)(Home)