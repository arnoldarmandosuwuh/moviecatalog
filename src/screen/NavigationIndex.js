import React, { Component } from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons"
import * as firebase from 'firebase'
import { connect } from 'react-redux'
import Home from './Home'
import Login from './Login'
import Register from './Register'
import Movie from './Movie'
import TvShow from './TvShow'
import About from './About'
import Detail from './Detail'

// Declare navigator
const Stack = createStackNavigator()
const Tabs = createBottomTabNavigator()

const MovieStackScreen = () => {
    return(
        <Stack.Navigator>
            <Stack.Screen name="Movie" component={Movie} options={{ title: 'Movie', headerLeft: null }}/>
        </Stack.Navigator>
    )
}

const DetailStackScreen = () => {
    return(
        <Stack.Navigator>
            <Stack.Screen name="Detail" component={Detail}/>
        </Stack.Navigator>
    )
}

const TvShowStackScreen = () => {
    return(
        <Stack.Navigator>
            <Stack.Screen name="TvShow" component={TvShow} options={{ title: 'Tv Show', headerLeft: null }}/>
        </Stack.Navigator>
    )
}

const AboutStackScreen = () => {
    return(
        <Stack.Navigator>
            <Stack.Screen name="About" component={About} options={{ title: 'About Us', headerLeft: null }}/>
        </Stack.Navigator>
    )  
}

const TabScreen = () => {
    return(
        <Tabs.Navigator
            screenOptions={({ route }) => ({
                tabBarIcon: ({ focused, color, size }) => {
                let iconName;
        
                if (route.name === 'Movie') {
                    iconName = focused
                    ? 'movie'
                    : 'movie-outline';
                } else if (route.name === 'TvShow') {
                    iconName = focused ? 'movie-open' : 'movie-open-outline';
                } else if (route.name === 'About') {
                    iconName = focused ? 'account-circle' : 'account-circle-outline';
                }
        
                // You can return any component that you like here!
                return <MaterialCommunityIcons name={iconName} size={size} color={color} />;
                },
            })}
            tabBarOptions={{
                activeTintColor: "#0BCAD4",
                style: {
                    backgroundColor: "#fff"
                },
                labelStyle: {
                    fontSize: 12,
                    marginBottom: 10,
                    fontWeight: "bold"
                },
            }}>
            <Tabs.Screen name="Movie" component={MovieStackScreen}/>
            <Tabs.Screen name="TvShow" component={TvShowStackScreen}/>
            <Tabs.Screen name="About" component={AboutStackScreen}/>
        </Tabs.Navigator>
    )
}

class NavigationIndex extends Component {
    constructor(props){
        super(props)
    }
    render = () => {
        return (
            this.props.todo.email ? (
                <Stack.Navigator 
                    initialRouteName="Movie" 
                        screenOptions={{
                            headerShown: false,
                        }}
                >
                    <Stack.Screen name="Movie" component={TabScreen}/>
                    <Stack.Screen name="Detail" component={DetailStackScreen}/>
                </Stack.Navigator>
            ) : (
                <Stack.Navigator 
                    initialRouteName="Home" 
                        screenOptions={{
                            headerShown: false,
                        }}
                >
                    <Stack.Screen name="Home" component={Home}/>
                    <Stack.Screen name="Login" component={Login}/>
                    <Stack.Screen name="Register" component={Register}/>
                </Stack.Navigator>
            )
        )
    }
}

const mapStateToProps = (state) => ({
    todo: state.todo
})

export default connect(mapStateToProps)(NavigationIndex)