import React, { Component } from 'react'
import { 
    StyleSheet, 
    Text, 
    View,
    Image,
    TouchableOpacity,
    TextInput,
    FlatList,
} from 'react-native'
import Movies from './component/MovieItem'
import axios from 'axios'
import { connect } from 'react-redux'
import { ADD_MOVIE } from '../constants/actionTypes'

class Movie extends Component {
    constructor(props){
        super(props)
        this.state = {
            data: [],
            baseUrl: 'https://api.themoviedb.org/3',
            apiKey: 'e852848561694f1300e3eac1deb19c49',
            detail: {}
        }
    }

    componentDidMount = () => {
        axios.get(`${this.state.baseUrl}/discover/movie?api_key=${this.state.apiKey}`)
            .then(res => {
                const data = res.data.results;
                this.setState({ data });
            })
    }

    detailMovie = (data) => {
        this.props.dispatch({
            type: ADD_MOVIE,
            payload: [data.title, data.release_date, data.popularity, data.vote_average, data.overview, data.poster_path]
        });
        this.props.navigation.navigate('Detail')
    }

    render = () => {
        return (
            <View style={styles.container}>
                <FlatList
                    data={this.state.data}
                    keyExtractor={(data) => data.id.toString()}
                    renderItem={(item) =>
                        <Movies 
                            movie={item.item} 
                            onPress={() => this.detailMovie(this.state.detail = item.item)} />}
                    ItemSeparatorComponent={() => <View style={{height:0.5,backgroundColor:'#E5E5E5'}}/>}
                />
            </View>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        justifyContent: 'center',
    },
})

const mapStateToProps = (state) => ({
    todo: state.todo
})

export default connect(mapStateToProps)(Movie)