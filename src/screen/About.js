import { StatusBar } from 'expo-status-bar'
import React, { Component } from 'react'
import { 
    StyleSheet, 
    Text, 
    View,
    Image,
    TouchableOpacity,
    TextInput,
} from 'react-native'
import Constants from 'expo-constants'

export default class About extends Component {

    render = () => {
        return (
            <View style={styles.container}>
                <Image source={require('../../assets/login.png')} style={styles.imageBackground}/>
                <View style={styles.header}>
                    <Image source={require('../../assets/title.png')} style={styles.logoImg}/>
                </View>
                <View style={{ flex: 1, justifyContent: 'center' }}>
                    <View style={styles.teamDev}>
                        <Image source={require('../../assets/foto_arnold.jpg')} style={styles.profileImage} />
                        <Text style={styles.textName}>Arnold Armando Suwuh</Text>
                        <Text style={styles.textJobs}>Android Developer</Text>
                    </View>
                    <View style={styles.teamDev}>
                        <Image source={require('../../assets/foto_marcel.jpg')} style={styles.profileImage} />
                        <Text style={styles.textName}>Marcel Martawidjaja</Text>
                        <Text style={styles.textJobs}>Android Developer</Text>
                    </View>
                </View>
                <StatusBar style="auto" />
            </View>

        )
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    imageBackground: {
        flex: 1,
        resizeMode: 'cover',
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        width: '100%',
		height: '100%'
    },
    header: {
        height: 50,
        marginVertical: 5,
        marginHorizontal: 24,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },

    logoImg: {
        height: 100,
        width: 300,
        resizeMode: 'contain',
    },

    teamDev: {
        alignItems: 'center',
        marginVertical: 10
    },

    profileImage: {
        width: 96,
        height: 96,
        borderRadius: 96 / 2,
    },

    textName: {
        fontSize: 24,
        fontWeight: 'bold',
        color: '#fff'
    },

    textTitle: {
        fontSize: 20,
        fontWeight: 'bold',
        color: '#4CFCF1',
        alignSelf: 'center'
    },

    textJobs: {
        fontSize: 16,
        color: '#4CFCF1'
    }
    
})