import { CREATE_SESSION, DESTROY_SESSION, ADD_USER, GET_USER, ADD_MOVIE, GET_MOVIE, DESTROY_MOVIE } from "../constants/actionTypes"

const initialState = {
  login: null,
  email: null,
  title: null,
  release_date: null,
  popularity: null,
  rating: null,
  overview: null,
  poster: null
}

const todoReducer  = (state = initialState, action) => {
    switch (action.type){
        case CREATE_SESSION:
            return{
                ...state,
                email: action.payload
            } 
            case DESTROY_SESSION:
                return {
                  ...state,
                  login: null,
                  email: null,
                }
            case ADD_USER:
                return {
                    ...state,
                    login: action.payload[0],
                    email: action.payload[1],
                }
            case GET_USER:
                return {
                    ...state,
                    email: action.payload
                }
            case ADD_MOVIE:
                return {
                    ...state,
                    title: action.payload[0],
                    release_date: action.payload[1],
                    popularity: action.payload[2],
                    rating: action.payload[3],
                    overview: action.payload[4],
                    poster: action.payload[5]
                }
            case GET_MOVIE: 
                return {
                    ...state,
                    title: action.payload[0],
                    release_date: action.payload[1],
                    population: action.payload[2],
                    rating: action.payload[3],
                    overview: action.payload[4],
                    poster: action.payload[5]
                }
            case DESTROY_MOVIE:
                return{
                    ...state,
                    title: null,
                    release_date: null,
                    population: null,
                    rating: null,
                    overview: null,
                    poster: null
                }
        default : 
            return state
    }
}

export default todoReducer