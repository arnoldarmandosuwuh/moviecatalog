export const CREATE_SESSION = 'CREATE_SESSION'
export const DESTROY_SESSION = 'DESTROY_SESSION'
export const ADD_USER = "ADD_USER"
export const GET_USER = "GET_USER"
export const ADD_MOVIE = "ADD_MOVIE"
export const GET_MOVIE = "GET_MOVIE"
export const DESTROY_MOVIE = "DESTROY_MOVIE"