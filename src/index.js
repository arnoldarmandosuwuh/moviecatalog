import React from 'react'
import { NavigationContainer } from '@react-navigation/native'
import NavigationIndex from './screen/NavigationIndex'
import { Provider } from 'react-redux'
import store from './store/store'

export default () => {
    return(
        <Provider store={store()}>
            <NavigationContainer>
                <NavigationIndex />
            </NavigationContainer>
        </Provider>
    )
}